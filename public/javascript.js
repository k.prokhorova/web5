function prices() {
    count = document.getElementById('count').value;
    price = document.getElementById('price').value;
    if (count.match(/^\d+$/) == null || price.match(/^\d+$/) == null) {
      result = 'Введены неверные символы.';
      document.getElementById('result').innerHTML = result;
    }
    else {
      result = parseInt (count)* parseInt (price);
      document.getElementById('result').innerHTML = "Стоимость равна: " + result + " р.";
    }
}